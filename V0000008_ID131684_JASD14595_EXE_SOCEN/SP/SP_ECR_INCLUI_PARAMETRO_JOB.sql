Set Ansi_Nulls Off
Set Quoted_Identifier Off
go
If Object_Id('SP_ECR_INCLUI_PARAMETRO_JOB', 'P') Is Null
	Exec ('Create Proc SP_ECR_INCLUI_PARAMETRO_JOB As Return 0;')
go
 

Alter Procedure SP_ECR_INCLUI_PARAMETRO_JOB     /*SP_ECR_INCLUI_PARAMETRO_JOB.sql*/

        @LOG_GERADOR_ID                 int                 , 
        @txt_query                      varchar(max)       , 
        @zUSUARIO_ID                    int                 
as 
        DECLARE @RETORNO                       int      , @TAB_TIPO_LAYOUT_iD INT           
		create table #temp_par(temp_Id int identity, item varchar(MAX) null)

		if exists (select 1 from log_gerador where funcao_Id = 2785 and log_gerador_ID = @LOG_GERADOR_ID)
		  begin
			INSERT #temp_par(ITEM)
			SELECT ITEMS FROM FN_SPLIT4(@txt_query, '__')

			SELECT @TAB_TIPO_LAYOUT_iD = C.TAB_TIPo_LAYOUT_iD
			FROM CLIENTE_EDI_FREQUENCIA_ENVIO A 
			INNER JOIN CLIENTE_EDI B ON A.CLIENTE_EDI_ID = B.CLIENTE_EDi_ID
			INNER JOIN LAYOUT C ON B.LAYOUT_IDENT = C.LAYOUT_IDENT 
			INNER JOIN #temp_par AS TMP ON TMP.ITEM = A.CLIENTE_EDI_FREQ_ENVIO_IDENT AND TMP.TEMP_iD = 9

		  end
		

        UPDATE LOG_GERADOR
        SET 
              txt_query                      = @txt_query          ,
			  TAB_TIPO_LAYOUT_iD		      = @TAB_TIPO_LAYOUT_iD
        WHERE LOG_GERADOR_ID                 = @LOG_GERADOR_ID                

        if @@rowcount = 0
            BEGIN
                SELECT  10 retorno, 'Nenhum registro afetado' msg_ret 
                RETURN 
            END

        if @@error <> 0
            BEGIN
                SELECT  error retorno, description msg_ret 
                FROM    master.dbo.sysmessages 
                WHERE   error = @@error 
                RETURN 
            END

        /* GRAVANDO A LOG NO SISTEMA: TABELA_SISTEMA_ID, USUARIO_ID, REGISTRO_ID, TAB_TIPO_ACAO_ID */
        /* RECEBE RETORNO PARA TER CERTEZA QUE GRAVOU */
        /* TABELA ACESSADA: LOG_GERADOR, seu ID eh 490 */
        EXEC SP_SIG_INC_LOG_SISTEMA2 490, @zUSUARIO_ID, 
                @LOG_GERADOR_ID, 2, @RETORNO OUTPUT 
        if @RETORNO <> 0
            BEGIN
                SELECT  @retorno retorno, 'Problemas na gravacao do registro de LOG' msg_ret 
                RETURN 
            END


        SELECT 0 retorno, '' msr_ret
		DROP TABLE #temp_par
go
Grant Execute On dbo.SP_ECR_INCLUI_PARAMETRO_JOB To Public
go
Grant Execute On dbo.SP_ECR_INCLUI_PARAMETRO_JOB To Sistema
go
