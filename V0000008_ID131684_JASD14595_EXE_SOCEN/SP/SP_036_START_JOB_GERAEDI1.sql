Set Ansi_Nulls On 
Set Quoted_Identifier On
go
If Object_Id('SP_036_START_JOB_GERAEDI1', 'P') Is Null
	Exec ('Create Proc SP_036_START_JOB_GERAEDI1 As Return 0;')
go
Alter Procedure SP_036_START_JOB_GERAEDI1                /*SP_036_START_JOB_GERAEDI1.sql*/
        @FUNCAO_ID                 		int                 , 
        @data_inicio                    DATETIME            , 
        @TAB_STATUS_JOB_ID              int 				, 
        @zUSUARIO_ID                    int					,
		@Tab_tipo_Layout_IDs			VARCHAR(2000)		,
		@Exceto_Tab_tipo_Layout_IDs		VARCHAR(2000)		,
		@EXE_GERADOR					VARCHAR(2000)
as 

    DECLARE @RETORNO            int                 
	DECLARE @LOG_GERADOR_ID		INT
 
	SELECT 
		@LOG_GERADOR_ID = MIN(LOG_GERADOR_ID)
	FROM 
		LOG_GERADOR with(nolock, forceseek)
	WHERE 
		TAB_STATUS_JOB_ID = 1 /* 'A' */
	AND FUNCAO_ID = @FUNCAO_ID
	AND (
			DATA_INICIO IS NULL 
			OR 
			DATA_INICIO <= GETDATE()
	    )
	AND 	
		(
			(ISNULL(TAB_TIPO_LAYOUT_ID,0) IN (SELECT ITEMS FROM DBO.FN_SPLIT5(@Tab_tipo_Layout_IDs,';')) AND ISNULL(@Tab_tipo_Layout_IDs,'') <> '') 
			or 
			ISNULL(@Tab_tipo_Layout_IDs,'') = ''
		)
	AND
		(
			(ISNULL(TAB_TIPO_LAYOUT_ID,0) NOT IN (SELECT ITEMS FROM DBO.FN_SPLIT5(@Exceto_Tab_tipo_Layout_IDs,';')) AND ISNULL(@Exceto_Tab_tipo_Layout_IDs,'') <> '') 
			or 
			ISNULL(@Exceto_Tab_tipo_Layout_IDs,'') = ''
		)

	IF @LOG_GERADOR_ID = 0 OR @LOG_GERADOR_ID IS NULL
	BEGIN
		/* Alterado retorno para 0 para parar de logar desnecessariamente */
	    SELECT  0 retorno, 'Não existe nenhum JOB agendado.' msg_ret, 0 LOG_GERADOR_ID 
        RETURN 
	END
	
    UPDATE 
		LOG_GERADOR
    SET 
		data_inicio                    = getdate()						, 
        TAB_STATUS_JOB_ID              = @TAB_STATUS_JOB_ID				,
		ARQUIVO_GERADO				   = ISNULL(Arquivo_Gerado,'') + ' - ' + ISNULL(@EXE_GERADOR,'Exe não informado')
    WHERE   
		LOG_GERADOR_ID                 = @LOG_GERADOR_ID                
		
    SELECT 0 retorno, '' msr_ret, @LOG_GERADOR_ID LOG_GERADOR_ID

go
Grant Execute On dbo.SP_036_START_JOB_GERAEDI1 To Public
go
Grant Execute On dbo.SP_036_START_JOB_GERAEDI1 To Sistema
go
