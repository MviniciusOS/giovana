Set Ansi_Nulls On 
Set Quoted_Identifier On
go
If Object_Id('SP_036_PROG_AGENDADOR_EDI14', 'P') Is Null
	Exec ('Create Proc SP_036_PROG_AGENDADOR_EDI14 As Return 0;')
go
Alter ProcEDURE SP_036_PROG_AGENDADOR_EDI14
AS

DECLARE 
    @CLIENTE_EDI_FREQ_ENVIO_IDENT       AS INT
    , @FREQ_ID                          AS INT
    , @TAB_DIA_SEMANA_ID                AS INT
    , @INTERVALO                        AS INT
    , @DIA_SEMANA_HOJE                  AS INT
    , @SOMA_SEMANA                      AS INT
    , @JOB_ID                           AS INT
    , @FUNCAO_ID                        AS INT
    , @CLIENTE_EDI_ID                   AS INT
    , @TAB_TIPO_LAYOUT_ID               AS INT
    , @TAB_TIPO_ARQ_RECEBIDO_ID         AS INT
    , @PESSOA_ID                        AS INT
    , @QUANT                            AS INT
    
DECLARE
    @HORA_ENVIO                         AS VARCHAR(5)
    , @HORA_DIA_SEMANA                  AS VARCHAR(5)
    , @INICIO_FREQUENTE                 AS VARCHAR(5)
    , @TERMINO_FREQUENTE                AS VARCHAR(5)
    , @HORA_ULT_EXECUCAO                AS VARCHAR(5)
    , @HORA_PROX_EXECUCAO               AS VARCHAR(5)
    , @AGORA                            AS VARCHAR(5)
    , @HORARIO                          AS VARCHAR(5)
    , @FLAG_ENVIO                       AS VARCHAR(1)
    , @FLAG_CONSIDERA                   AS VARCHAR(1)
    , @PREFIXO_NOME_ARQ                 AS VARCHAR(50)
    , @EMAIL_DEST                       AS VARCHAR(45)
    , @EMAIL_REM                        AS VARCHAR(100)
    , @DIRETORIO_ORIGEM                 AS VARCHAR(100)
    , @CPF_CGC                          AS VARCHAR(15)
    , @INDICATIVO_PF_PJ                 AS VARCHAR(2)
    , @TXT_QUERY                        AS VARCHAR(4000)

DECLARE
    @PROXIMA_EXECUCAO_PROGRAMADA        AS DATETIME
    , @ULT_EXECUCAO                     AS DATETIME
    , @PROX_EXECUCAO                    AS DATETIME
    , @HOJE                             AS DATETIME
	, @VAR_EXEC							AS VARCHAR(4000)
	, @TEMPLATE_ID						AS INT
	, @SP_TEMPLATE						AS VARCHAR(100)
	, @MENSAGEM							AS VARCHAR(7000)
	, @TAB_STATUS_FILTRO				AS VARCHAR(5)
	, @TITULO_EMAIL						AS VARCHAR(255)
	, @DATA_DE							AS VARCHAR(10)
	, @DATA_ATE							AS VARCHAR(10)
	, @TAB_DIA_SEMANA_QUINZENA_ID		AS INT
	, @HORA_ENVIO_QUINZENA				AS VARCHAR(5)
	, @DIA_ENVIO_MES					AS INT
	, @HORA_ENVIO_MES					AS VARCHAR(5)
	, @AUX_DATA_AGEND					AS DATETIME
	, @AUX_GRAVA_AGEND					AS CHAR(1)
	, @DESTINATARIO_ARQ					AS VARCHAR(255)

/*=======================================================================================================================*/
    
    SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
    
    SELECT @HOJE = parametro 
    FROM tab_parametro_sistema (nolock) 
    WHERE tab_parametro_sistema_id = 3557

    IF @HOJE IS NULL
        SET @HOJE = GETDATE()
    ELSE
        SET @HOJE = CONVERT(VARCHAR(10), @HOJE, 111)
    
    /* E-MAIL DO REMETENTE PARA ARQUIVO SEFAZ */
    SELECT @EMAIL_REM = email
    FROM empresa (nolock)
    WHERE empresa_id = 1

    /************************************************************************************************************************/
    /* VERIFICAR SE EXISTEM AGENDAMENTOS COM ULTIMA EXECU��O PROGRAMADA ANTERIOR A DATA/HORA DO SERVIDOR, N�O EXECUTADOS    */
    /* ULTIMA_EXECUCAO_PROGRAMADA < GETDATE()                                        */
    /* PROXIMA_EXECUCAO_PROGRAMADA < GETDATE()                                        */
    /************************************************************************************************************************/
    DECLARE CURSOR_EDI CURSOR LOCAL STATIC FORWARD_ONLY FOR
        SELECT 
            a.cliente_edi_freq_envio_ident
        FROM cliente_edi_frequencia_envio AS a
            INNER JOIN cliente_edi AS b 
                ON b.cliente_edi_id = a.cliente_edi_id
                AND b.tab_status_id = 1
        WHERE 
        (
            a.proxima_execucao_programada < @HOJE 
            OR 
            a.proxima_execucao_programada IS NULL
        )
        AND a.tab_status_id = 1

    OPEN CURSOR_EDI
    
    FETCH NEXT FROM CURSOR_EDI 
        INTO @CLIENTE_EDI_FREQ_ENVIO_IDENT
    
    WHILE (@@FETCH_STATUS = 0)
        BEGIN
            SELECT    
                @FREQ_ID = a.frequencia_envio_id
                , @HORA_ENVIO = a.hora_envio
                , @TAB_DIA_SEMANA_ID = a.tab_dia_semana_id
                , @HORA_DIA_SEMANA = a.hora_dia_semana
                , @INICIO_FREQUENTE = a.inicio_frequente
                , @TERMINO_FREQUENTE = a.termino_frequente
                , @INTERVALO = a.intervalo
                , @PROXIMA_EXECUCAO_PROGRAMADA = a.proxima_execucao_programada
                , @CLIENTE_EDI_ID = a.cliente_edi_id
				, @TAB_DIA_SEMANA_QUINZENA_ID = a.TAB_DIA_SEMANA_QUINZENA_ID
				, @HORA_ENVIO_QUINZENA = ISNULL(a.HORA_ENVIO_QUINZENA,'00:00')
				, @DIA_ENVIO_MES = ISNULL(a.DIA_ENVIO_MES,1)
				, @HORA_ENVIO_MES = ISNULL(a.HORA_ENVIO_MES,'00:00')
				, @DESTINATARIO_ARQ = cliente_edi.EMAIL
            FROM cliente_edi_frequencia_envio AS a
                LEFT JOIN tab_dia_semana AS b
                    ON a.tab_dia_semana_id = b.tab_dia_semana_id
				LEFT JOIN cliente_edi
					ON cliente_edi.cliente_edi_id = A.cliente_edi_id
            WHERE a.cliente_edi_freq_envio_ident = @CLIENTE_EDI_FREQ_ENVIO_IDENT
            
            /* HORA */
            SET @AGORA = CONVERT(VARCHAR, @HOJE, 114)
            
            SET @FLAG_CONSIDERA = 'S'

            IF @PROXIMA_EXECUCAO_PROGRAMADA IS NULL
                BEGIN
                    SET @PROXIMA_EXECUCAO_PROGRAMADA = GETDATE()
                    SET @DIA_SEMANA_HOJE = DATEPART(DW, @HOJE)
                END
            ELSE
                SET @DIA_SEMANA_HOJE = DATEPART(DW, @PROXIMA_EXECUCAO_PROGRAMADA)
            
            SET @ULT_EXECUCAO = @PROXIMA_EXECUCAO_PROGRAMADA
            
            /* SEMANAL */
            IF @FREQ_ID = 1
                BEGIN
                    IF @DIA_SEMANA_HOJE < @TAB_DIA_SEMANA_ID 
                        SET @SOMA_SEMANA = 0
                        
                    IF @DIA_SEMANA_HOJE > @TAB_DIA_SEMANA_ID 
                        SET @SOMA_SEMANA = 7
                        
                    IF @DIA_SEMANA_HOJE = @TAB_DIA_SEMANA_ID 
                        BEGIN
                            IF @HORA_DIA_SEMANA > @AGORA
                                SET @SOMA_SEMANA = 0
                            ELSE
                                SET @SOMA_SEMANA = 7
                        END
                        
                    SET @PROX_EXECUCAO = DATEADD(DD, @TAB_DIA_SEMANA_ID + @SOMA_SEMANA - @DIA_SEMANA_HOJE, @HOJE)
                    SET @HORA_PROX_EXECUCAO = @HORA_DIA_SEMANA
               END

            /* DI�RIO */
            IF @FREQ_ID = 2
                BEGIN
                    IF @HORA_ENVIO > @AGORA
                        SET @PROX_EXECUCAO = @HOJE
                    ELSE
                        SET @PROX_EXECUCAO = DATEADD(DD, 1, @HOJE)

                    SET @HORA_PROX_EXECUCAO = @HORA_ENVIO
                END
        
            /* FREQUENTE */
            IF @FREQ_ID = 3
                BEGIN
                    /* QUANDO FOR FREQUENTE, INCLUI JOB APENAS NO PER�ODO INFORMADO */
                    IF @AGORA BETWEEN @INICIO_FREQUENTE AND @TERMINO_FREQUENTE
                        BEGIN
                            /* UTILIZA DATA DE EXECUCAO DA SP PARA CALCULAR A PROXIMA EXECUCAO */
                            SET @ULT_EXECUCAO = GETDATE()
                            SET @PROX_EXECUCAO = DATEADD(MI, @INTERVALO, @ULT_EXECUCAO)
                            
                            /* APENAS HORA */
                            SET @HORA_PROX_EXECUCAO = CONVERT(CHAR(5), @PROX_EXECUCAO, 114)
                        END
                    ELSE
                  SET @FLAG_CONSIDERA = 'N'
                END

            /* ON-DEMAND */
            IF @FREQ_ID = 4
                SET @FLAG_CONSIDERA = 'N'
                
            /* QUINZENAL */
            IF @FREQ_ID = 5
                BEGIN
                    IF DAY(@HOJE) < 16
                        BEGIN
                            --PRINT '1A QUINZENA'
                            SET @PROX_EXECUCAO = CAST(CAST(YEAR(@HOJE) AS VARCHAR) + '-' + CAST(MONTH(@HOJE) AS  VARCHAR) + '-16' AS DATETIME)
                        END
                    ELSE
                        BEGIN
                            --PRINT '2a QUINZENA'
                            SET @PROX_EXECUCAO = CAST(CAST(YEAR(@HOJE) AS VARCHAR) + '-' + CAST(MONTH(@HOJE) AS  VARCHAR) + '-01' AS DATETIME)
                            SET @PROX_EXECUCAO = DATEADD(MONTH, 1, @PROX_EXECUCAO)
                        END                    
                
                    SET @HORA_PROX_EXECUCAO = '00:00'
                END
            
            /* MENSAL */
            IF @FREQ_ID = 6
                BEGIN
                    SET @PROX_EXECUCAO = CAST(CAST(YEAR(@HOJE) AS VARCHAR) + '-' + CAST(MONTH(@HOJE) AS  VARCHAR) + '-01' AS DATETIME)
                    SET @PROX_EXECUCAO = DATEADD(MONTH, 1, @PROX_EXECUCAO)
                    SET @HORA_PROX_EXECUCAO = '00:00'
                END
            
            /* SO GRAVA JOB SE OCORREU ALGUM ERRO OU FINALIZADO COM SUCESSO */
            SELECT
                @FLAG_ENVIO = ttl.flag_envio
                , @TAB_TIPO_LAYOUT_ID = ttl.tab_tipo_layout_id
                , @PREFIXO_NOME_ARQ = ce.prefixo_nome_arq
                , @PESSOA_ID = ce.pessoa_id
                , @EMAIL_DEST = ce.email
                , @DIRETORIO_ORIGEM = ce.diretorio_origem
                , @FUNCAO_ID = ttl.funcao_id
            FROM cliente_edi AS ce
                INNER JOIN layout AS l
                    ON l.layout_ident = ce.layout_ident
                    AND l.tab_status_id = 1
                INNER JOIN tab_tipo_layout AS ttl
                    ON ttl.tab_tipo_layout_id = l.tab_tipo_layout_id
            WHERE ce.cliente_edi_id = @CLIENTE_EDI_ID
            AND ce.tab_status_id = 1

            /* 22/05/2007 - IND�STRIA INCENTIVADA, N�O INCENTIVADA E RESENDE (SEFAZ) */
            IF @TAB_TIPO_LAYOUT_ID IN(14,15,16) 
                BEGIN
                    IF @FLAG_CONSIDERA = 'S' AND NOT EXISTS(SELECT * FROM Log_Gerador WITH(FORCESEEK)
                        WHERE funcao_id = 2244
                        AND txt_query LIKE '__JOB__%__%__%__%__%__%__%__' + CONVERT(VARCHAR, @CLIENTE_EDI_FREQ_ENVIO_IDENT) + '__%'
                        AND tab_status_job_id = 1)
                        
                        BEGIN
                            /* VALORIZA��O DA @TAB_TIPO_ARQ_RECEBIDO_ID DE ACORDO COM O TIPO DE LAYOUT */
                            IF @TAB_TIPO_LAYOUT_ID = 14
                                SET @TAB_TIPO_ARQ_RECEBIDO_ID = 28
                            ELSE
                                BEGIN
                                    IF @TAB_TIPO_LAYOUT_ID = 15
                                        SET @TAB_TIPO_ARQ_RECEBIDO_ID = 29
                                    ELSE
                                        SET @TAB_TIPO_ARQ_RECEBIDO_ID = 30
                                END

                            /* RECUPERA O CPF/CGC DO EMBARCADOR */
                            SELECT     
                                @CPF_CGC = 
                                    CASE 
                                        WHEN indicativo_pf_pj = 'PF' THEN
                                            pf_cpf
                                        ELSE
                                            pj_cgc
                                    END
                            FROM pessoa (nolock)
                      WHERE pessoa_id = @PESSOA_ID                            

                            UPDATE cliente_edi_frequencia_envio
                                SET
                                    ultima_execucao_programada = proxima_execucao_programada
                                    , proxima_execucao_programada = CONVERT(DATETIME, (CONVERT(VARCHAR(10), @PROX_EXECUCAO, 111) + ' ' + CONVERT(VARCHAR(5), @HORA_PROX_EXECUCAO)))
                            WHERE cliente_edi_freq_envio_ident = @CLIENTE_EDI_FREQ_ENVIO_IDENT
                            
                            UPDATE ult_infra_id 
                                SET 
                                    ultimo_id = ultimo_id + 1
                                    , @JOB_ID = ultimo_id + 1
                            WHERE tabela_sistema_id = 490

                            SET @TXT_QUERY = ''
                            SET @TXT_QUERY = @TXT_QUERY + '__JOB__' + CONVERT(VARCHAR, @JOB_ID) + '__'
                            SET @TXT_QUERY = @TXT_QUERY + CONVERT(VARCHAR(100), @PREFIXO_NOME_ARQ) + '__'
                            SET @TXT_QUERY = @TXT_QUERY + CONVERT(VARCHAR(100), @TAB_TIPO_ARQ_RECEBIDO_ID) + '__'
                            
                            /* EMBARCADOR_ID */
                            SET @TXT_QUERY = @TXT_QUERY + '__'
                            SET @TXT_QUERY = @TXT_QUERY + CONVERT(VARCHAR(100), @EMAIL_DEST) + '__'
                            SET @TXT_QUERY = @TXT_QUERY + CONVERT(VARCHAR(100), @EMAIL_REM) + '__'
                            SET @TXT_QUERY = @TXT_QUERY + 'Anexo a log gerada pela absorção automática dos arquivos de nota fiscal EDI__'
                            SET @TXT_QUERY = @TXT_QUERY + CONVERT(VARCHAR, @CLIENTE_EDI_FREQ_ENVIO_IDENT) + '__'
                            
                            /* INDIC_ABSORCAO_AUTOMATICA */
                            SET @TXT_QUERY = @TXT_QUERY + 'S' + '__' 
                            SET @TXT_QUERY = @TXT_QUERY + CONVERT(VARCHAR, @CPF_CGC) + '__'
                            SET @TXT_QUERY = @TXT_QUERY + CONVERT(VARCHAR(100), @DIRETORIO_ORIGEM)
                            
                            INSERT INTO log_gerador 
                             (
                                log_gerador_id
                                , funcao_id
                                , data_inicio
                                , tab_status_job_id
                                , txt_query
								, tab_tipo_layout_id
                            )
                            VALUES
                            (
                                @JOB_ID
                                , 2244
                                , CONVERT(DATETIME, (CONVERT(VARCHAR(10), @PROX_EXECUCAO, 111) + ' ' + CONVERT(VARCHAR(5), @HORA_PROX_EXECUCAO)))
                                , 1
                                , @TXT_QUERY
								, @TAB_TIPO_LAYOUT_ID
                            )
                       END
                END
            ELSE
                BEGIN
					IF @TAB_TIPO_LAYOUT_ID IN(92, 93, 94, 95, 96, 97, 107, 108, 109, 110)
						BEGIN
							SELECT @TEMPLATE_ID = TEMPLATE_ID, @MENSAGEM = MENSAGEM from tab_tipo_layout where tab_tipo_layout_id = @TAB_TIPO_LAYOUT_ID
							SELECT @SP_TEMPLATE = NOME_SP FROM TEMPLATE WHERE TEMPLATE_ID = @TEMPLATE_ID
							SET @TITULO_EMAIL = 'Relatório Averbação Seguro - Mercadorias na Apólice - '

							SET @AUX_GRAVA_AGEND = 'N'

							IF @FREQ_ID = 2 --Di�rio
							 BEGIN
								SET @DATA_DE = CONVERT(VARCHAR,getdate()-1,111)
								SET @DATA_ATE = CONVERT(VARCHAR,getdate()-1,111)
								SET @AUX_GRAVA_AGEND = 'S'
							 END

							/* QUINZENAL */
							IF @FREQ_ID = 5
							 BEGIN
								IF (DAY(@HOJE) >= 1 AND DAY(@HOJE) <= 15) OR (DAY(@HOJE) >= 16 AND DAY(@HOJE) <= 22)
								 BEGIN
									SET @DIA_SEMANA_HOJE = DATEPART(weekday, getdate())
									IF ISNULL(@TAB_DIA_SEMANA_QUINZENA_ID,0)=0
									 BEGIN
										SELECT @AUX_DATA_AGEND = GETDATE()
									 END
									ELSE
									 BEGIN
										IF @DIA_SEMANA_HOJE<=@TAB_DIA_SEMANA_QUINZENA_ID
										 BEGIN
											SELECT @AUX_DATA_AGEND = GETDATE()+(@TAB_DIA_SEMANA_QUINZENA_ID-@DIA_SEMANA_HOJE)
										 END
										ELSE
										 BEGIN
											SELECT @AUX_DATA_AGEND = GETDATE()-(@DIA_SEMANA_HOJE-@TAB_DIA_SEMANA_QUINZENA_ID)
										 END
									 END


									SET @PROX_EXECUCAO = CAST(convert(varchar,@AUX_DATA_AGEND,111) AS DATETIME)
									SET @HORA_PROX_EXECUCAO = @HORA_ENVIO_QUINZENA

									IF DAY(@HOJE) < 16
									 BEGIN
										--PRINT '1A QUINZENA'
										SET @DATA_DE = CONVERT(VARCHAR,YEAR(DATEADD(month, -1, getdate()))) + '/' + CONVERT(VARCHAR,MONTH(DATEADD(month, -1, getdate()))) + '/16'
										SET @DATA_ATE = CONVERT(VARCHAR,YEAR(DATEADD(month, +1, DATEADD(month, -1, getdate())))) + '/' + CONVERT(VARCHAR,MONTH(DATEADD(month, +1, DATEADD(month, -1, getdate())))) + '/01'
									 END
									ELSE
									 BEGIN
										--PRINT '2a QUINZENA'
										SET @DATA_DE = CONVERT(VARCHAR,YEAR(getdate())) + '/' + CONVERT(VARCHAR,MONTH(getdate())) + '/01'
										SET @DATA_ATE = CONVERT(VARCHAR,YEAR(getdate())) + '/' + CONVERT(VARCHAR,MONTH(getdate())) + '/16'
									 END
									SET @AUX_GRAVA_AGEND = 'S'
								 END
							 END

							/* MENSAL */
							IF @FREQ_ID = 6
							 BEGIN
								--IF DAY(@HOJE)>=1 AND DAY(@HOJE)<=15
								 --BEGIN
									SET @PROX_EXECUCAO = CAST(CAST(YEAR(getdate()) AS VARCHAR) + '-' + CAST(MONTH(getdate()) AS  VARCHAR) + '-' + CONVERT(VARCHAR,@DIA_ENVIO_MES) AS DATETIME)
									SET @HORA_PROX_EXECUCAO = @HORA_ENVIO_MES

									SET @DATA_DE = CONVERT(VARCHAR,YEAR(DATEADD(month, -1, getdate()))) + '/' + CONVERT(VARCHAR,MONTH(DATEADD(month, -1, getdate()))) + '/01'
									SET @DATA_ATE = CONVERT(VARCHAR,YEAR(getdate())) + '/' + CONVERT(VARCHAR,MONTH(getdate())) + '/01'

									SET @AUX_GRAVA_AGEND = 'S'
								 --END
							 END

							IF @TAB_TIPO_LAYOUT_ID IN(92)
							 BEGIN
								SET @TAB_STATUS_FILTRO = '999'
								SET @TITULO_EMAIL = @TITULO_EMAIL + 'Emitidas'
							 END
							IF @TAB_TIPO_LAYOUT_ID IN(95)
							 BEGIN
								SET @TAB_STATUS_FILTRO = '999'
								SET @TITULO_EMAIL = @TITULO_EMAIL + 'Emitidas - Cargas Excluídas'
							 END
							IF @FREQ_ID = 2 --Di�rio
							 BEGIN
								SET @MENSAGEM = REPLACE(@MENSAGEM,'DD/MM/AAAA',CONVERT(VARCHAR,getdate()-1,103))
							 END

							IF @TAB_TIPO_LAYOUT_ID IN(93)
							 BEGIN
								SET @TAB_STATUS_FILTRO = '2'
								SET @TITULO_EMAIL = @TITULO_EMAIL + 'Canceladas'
							 END
							IF @TAB_TIPO_LAYOUT_ID IN(96)
							 BEGIN
								SET @TAB_STATUS_FILTRO = '2'
								SET @TITULO_EMAIL = @TITULO_EMAIL + 'Canceladas - Cargas Excluídas'
							 END
							IF @FREQ_ID = 5
							 BEGIN
								IF @DATA_DE<>''
									SET @MENSAGEM = REPLACE(@MENSAGEM,'de DD/MM/AAAA', 'de ' + CONVERT(VARCHAR,CAST(@DATA_DE AS DATETIME),103))
								IF @DATA_ATE<>''
									SET @MENSAGEM = REPLACE(@MENSAGEM,'até DD/MM/AAAA', 'até '+ CONVERT(VARCHAR,CAST(@DATA_ATE AS DATETIME)-1,103))
							 END

							IF @TAB_TIPO_LAYOUT_ID IN(94)
							 BEGIN
								SET @TAB_STATUS_FILTRO = '1001'
								SET @TITULO_EMAIL = @TITULO_EMAIL + 'Alteradas'
							 END
							IF @TAB_TIPO_LAYOUT_ID IN(97)
							 BEGIN
								SET @TAB_STATUS_FILTRO = '1001'
								SET @TITULO_EMAIL = @TITULO_EMAIL + 'Alteradas - Cargas Excluídas'
							 END

							 /*In�cio - Filtro de Averba��o*/
							 IF @TAB_TIPO_LAYOUT_ID IN(107)
							 BEGIN
								SET @TAB_STATUS_FILTRO = '1'
								SET @TITULO_EMAIL = @TITULO_EMAIL + 'Provisória Ativa'
							 END
							 IF @TAB_TIPO_LAYOUT_ID IN(108)
							 BEGIN
								SET @TAB_STATUS_FILTRO = '2'
								SET @TITULO_EMAIL = @TITULO_EMAIL + 'Provisória Cancelada'
							 END
							 IF @TAB_TIPO_LAYOUT_ID IN(109)
							 BEGIN
								SET @TAB_STATUS_FILTRO = '3'
								SET @TITULO_EMAIL = @TITULO_EMAIL + 'Definitiva Ativa'
							 END
							 IF @TAB_TIPO_LAYOUT_ID IN(110)
							 BEGIN
								SET @TAB_STATUS_FILTRO = '4'
								SET @TITULO_EMAIL = @TITULO_EMAIL + 'Definitiva Cancelada'
							 END
							 /*Fim - Filtro de Averba��o*/
							 							 
							IF @FREQ_ID = 6
							 BEGIN
								IF @DATA_DE<>''
									SET @MENSAGEM = REPLACE(@MENSAGEM,'de DD/MM/AAAA', 'de ' + CONVERT(VARCHAR,CAST(@DATA_DE AS DATETIME),103))
								IF @DATA_ATE<>''
									SET @MENSAGEM = REPLACE(@MENSAGEM,'até DD/MM/AAAA', 'até '+ CONVERT(VARCHAR,CAST(@DATA_ATE AS DATETIME)-1,103))
							 END

							SELECT 
								@QUANT = COUNT(log_gerador_id) 
							FROM log_gerador WITH(FORCESEEK)
							WHERE funcao_id IN(2785)
							AND txt_query LIKE '__JOB__%__E__' + CONVERT(VARCHAR, @CLIENTE_EDI_FREQ_ENVIO_IDENT) + '__%'
							AND tab_status_job_id = 1
 
							--- s� gravo a log_gerador se a proxima execu��o tem que ser executada no dia da inclus�o
							IF @QUANT = 0 AND @AUX_GRAVA_AGEND = 'S' and convert(varchar,@PROX_EXECUCAO,111) = convert(varchar,@HOJE,111)
								BEGIN
									IF @FREQ_ID = 5
									 BEGIN
										IF ISNULL(@TAB_DIA_SEMANA_QUINZENA_ID,0)=0
										 BEGIN
											UPDATE cliente_edi_frequencia_envio
												SET
													ultima_execucao_programada = proxima_execucao_programada
													, proxima_execucao_programada = CONVERT(DATETIME, (CONVERT(VARCHAR(10), DATEADD(day, 15, @PROX_EXECUCAO), 111) + ' ' + CONVERT(VARCHAR(5), @HORA_PROX_EXECUCAO)))
											WHERE cliente_edi_freq_envio_ident = @CLIENTE_EDI_FREQ_ENVIO_IDENT
										 END
										ELSE
										 BEGIN
											IF DATEPART(DW, DATEADD(day, 15, getdate()))<=@TAB_DIA_SEMANA_QUINZENA_ID
											 BEGIN
												SELECT @AUX_DATA_AGEND = DATEADD(day, 15, getdate())+(@TAB_DIA_SEMANA_QUINZENA_ID-DATEPART(DW, DATEADD(day, 15, getdate())))
											 END
											ELSE
											 BEGIN
												SELECT @AUX_DATA_AGEND = DATEADD(day, 15, getdate())-(DATEPART(DW, DATEADD(day, 15, getdate()))-@TAB_DIA_SEMANA_QUINZENA_ID)
											 END

											UPDATE cliente_edi_frequencia_envio
												SET
													ultima_execucao_programada = proxima_execucao_programada
													, proxima_execucao_programada = CONVERT(DATETIME, (CONVERT(VARCHAR(10), @AUX_DATA_AGEND,111 ) + ' ' + CONVERT(VARCHAR(5), @HORA_PROX_EXECUCAO)))
											WHERE cliente_edi_freq_envio_ident = @CLIENTE_EDI_FREQ_ENVIO_IDENT

										 END
									 END
									ELSE
									 BEGIN
										IF @FREQ_ID = 6
										BEGIN
											UPDATE cliente_edi_frequencia_envio
												SET
													ultima_execucao_programada = proxima_execucao_programada
													, proxima_execucao_programada = CONVERT(DATETIME, (CONVERT(VARCHAR(10), DATEADD(MONTH, 1, @PROX_EXECUCAO), 111) + ' ' + CONVERT(VARCHAR(5), @HORA_PROX_EXECUCAO)))
											WHERE cliente_edi_freq_envio_ident = @CLIENTE_EDI_FREQ_ENVIO_IDENT
										END
										ELSE
										BEGIN
											UPDATE cliente_edi_frequencia_envio
												SET
													ultima_execucao_programada = proxima_execucao_programada
													, proxima_execucao_programada = CONVERT(DATETIME, (CONVERT(VARCHAR(10), @PROX_EXECUCAO, 111) + ' ' + CONVERT(VARCHAR(5), @HORA_PROX_EXECUCAO)))
											WHERE cliente_edi_freq_envio_ident = @CLIENTE_EDI_FREQ_ENVIO_IDENT
										END
									 END

									UPDATE ult_infra_id 
										SET 
											@JOB_ID = ultimo_id + 1
											, ultimo_id = ultimo_id + 1
									WHERE tabela_sistema_id = 490

									/* 23/08/2010 -  para evitar a duplica��o da log_gerador_id, verifico se existe, caso exista consulto outra log_gerador_id */
									IF EXISTS(SELECT * FROM log_gerador WHERE log_gerador_id = @JOB_ID)
										BEGIN
											/* AGUARDA 1 MILISEGUNDO PARA RECUPERAR OUTRO id */
											WAITFOR DELAY '000:00:01'

											UPDATE ult_infra_id 
												SET 
													@JOB_ID = ultimo_id + 1
													, ultimo_id = ultimo_id + 1
											WHERE tabela_sistema_id = 490
										END
						    
									IF @TAB_TIPO_LAYOUT_ID IN(92, 93, 94) 
										SET @VAR_EXEC = 'EXEC ' + @SP_TEMPLATE + ' null, ##' + @DATA_DE + '##, ##' + @DATA_ATE + '##, ' + @TAB_STATUS_FILTRO + ', ##S##, ##S##, ##todas##, null, null, null, null, ##N##, null, ##A##' /*Filtro de OS*/
									ELSE
										BEGIN
											IF @TAB_TIPO_LAYOUT_ID IN(107, 108, 109, 110) 
												SET @VAR_EXEC = 'EXEC ' + @SP_TEMPLATE + ' null, null, null, null , ##S##, ##S##, ##todas##, null, ' + @TAB_STATUS_FILTRO + ', ##' + @DATA_DE + '##, ##' + @DATA_ATE + '##, ##N##, null, ##A##' /*Filtro de Averba��o*/
											ELSE 
												SET @VAR_EXEC = 'EXEC ' + @SP_TEMPLATE + ' null, ##' + @DATA_DE + '##, ##' + @DATA_ATE + '##, ' + @TAB_STATUS_FILTRO + ', ##N##, ##S##, ##todas##, null, null, null, null, ##N##, null, ##A##' /*Filtro de OS*/
										END


									INSERT INTO log_gerador 
									(
										log_gerador_id
										, funcao_id
										, data_inicio
										, txt_query
										, tab_status_job_id
										, TEXTO_EMAIL
										, TITULO_EMAIL
										, FLAG_ENVIA_EMAIL
										, ENDERECO_EMAIL
										, tab_tipo_layout_id
									)
									VALUES
									(
										@JOB_ID
										, @FUNCAO_ID
										, CONVERT(DATETIME, (CONVERT(VARCHAR(10), @PROX_EXECUCAO, 111) + ' ' + CONVERT(VARCHAR(5), @HORA_PROX_EXECUCAO)))
										, '__JOB__' + CONVERT(VARCHAR, @JOB_ID) + '__E__' + CONVERT(VARCHAR, @CLIENTE_EDI_FREQ_ENVIO_IDENT) + '__' + @VAR_EXEC
										, 1
										, @MENSAGEM
										, @TITULO_EMAIL
										, 'S'
										, @DESTINATARIO_ARQ
										, @TAB_TIPO_LAYOUT_ID
									)

								END
						END
					ELSE
						BEGIN
							SELECT @QUANT = COUNT(log_gerador_id) 
							FROM log_gerador WITH(FORCESEEK)
							WHERE funcao_id IN(2785,2786,2964)
							AND txt_query LIKE '__JOB__%__N__' + CONVERT(VARCHAR, @CLIENTE_EDI_FREQ_ENVIO_IDENT)
							AND tab_status_job_id = 1
                    
							IF ((@FREQ_ID = 3 AND @INTERVALO < 5 AND @QUANT <= 3 ) OR (@QUANT = 0)) AND @FLAG_CONSIDERA = 'S'
								BEGIN
									IF @QUANT = 0
										UPDATE cliente_edi_frequencia_envio
											SET
												ultima_execucao_programada = proxima_execucao_programada
												, proxima_execucao_programada = CONVERT(DATETIME, (CONVERT(VARCHAR(10), @PROX_EXECUCAO, 111) + ' ' + CONVERT(VARCHAR(5), @HORA_PROX_EXECUCAO)))
										WHERE cliente_edi_freq_envio_ident = @CLIENTE_EDI_FREQ_ENVIO_IDENT

									UPDATE ult_infra_id 
										SET 
											@JOB_ID = ultimo_id + 1
											, ultimo_id = ultimo_id + 1
									WHERE tabela_sistema_id = 490
		  
									/* 23/08/2010 -  para evitar a duplica��o da log_gerador_id, verifico se existe, caso exista consulto outra log_gerador_id */
									IF EXISTS(SELECT * FROM log_gerador WHERE log_gerador_id = @JOB_ID)
										BEGIN
											/* AGUARDA 1 MILISEGUNDO PARA RECUPERAR OUTRO id */
											WAITFOR DELAY '000:00:01'

											UPDATE ult_infra_id 
												SET 
													@JOB_ID = ultimo_id + 1
													, ultimo_id = ultimo_id + 1
											WHERE tabela_sistema_id = 490
										END
		                            
									INSERT INTO log_gerador 
									(
										log_gerador_id
										, funcao_id
										, data_inicio
										, txt_query
										, tab_status_job_id
										, tab_tipo_layout_id
									)
									VALUES
									(
										@JOB_ID
										, @FUNCAO_ID
										, CONVERT(DATETIME, (CONVERT(VARCHAR(10), @PROX_EXECUCAO, 111) + ' ' + CONVERT(VARCHAR(5), @HORA_PROX_EXECUCAO)))
										, '__JOB__' + CONVERT(VARCHAR, @JOB_ID) + '__N__' + CONVERT(VARCHAR, @CLIENTE_EDI_FREQ_ENVIO_IDENT)
										, 1
										,@TAB_TIPO_LAYOUT_ID
									)
								END
						END
                END
            
            SET @TAB_TIPO_LAYOUT_ID = NULL
            SET @TAB_TIPO_LAYOUT_ID = NULL
            SET @PREFIXO_NOME_ARQ = NULL
            SET @PESSOA_ID = NULL
            SET @EMAIL_DEST = NULL
            SET @DIRETORIO_ORIGEM = ''
            SET @FUNCAO_ID = NULL

            FETCH NEXT FROM CURSOR_EDI 
                INTO @CLIENTE_EDI_FREQ_ENVIO_IDENT
        END

    CLOSE CURSOR_EDI
    DEALLOCATE CURSOR_EDI
    
    /******************   ATUALIZA PARAMETRO DO SISTEMA - ATRIBUINDO VALOR NULO PARA A DATA(PARAMETRO)   ******************/
    UPDATE tab_parametro_sistema 
        SET 
            parametro = NULL 
	WHERE tab_parametro_sistema_id = 3557
    
    SET NOCOUNT OFF
 
 
go
Grant Execute On dbo.SP_036_PROG_AGENDADOR_EDI14 To Public
go
Grant Execute On dbo.SP_036_PROG_AGENDADOR_EDI14 To Sistema
go
