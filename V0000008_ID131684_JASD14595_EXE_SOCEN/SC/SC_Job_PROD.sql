DECLARE 
	@var_jobId BINARY(16)
	, @nome_job VARCHAR(255)

SET @nome_job = N'Batch Insere Jobs EDI'

select @var_jobId = job_id from msdb.dbo.sysjobs  where name like @nome_job

update msdb.dbo.sysjobsteps set command = ('EXECUTE SP_036_PROG_AGENDADOR_EDI14')  where step_name like N'sp_ecr_prog_agendador_edi' and job_id = @var_jobId AND  database_name = 'ALIANCAPROD'