If Not Exists (Select 1 From sys.columns Where Name = 'Arquivo_Gerado' And [object_Id] = Object_Id('LOG_GERADOR'))
	Alter Table [LOG_GERADOR] Add [Arquivo_Gerado] varchar(2000) Null
Else 
	If Not Exists(Select * From sys.columns c Where c.[object_id] = Object_Id('LOG_GERADOR') And c.Name = 'Arquivo_Gerado' And Type_Name(c.system_type_id) = 'varchar' And c.max_length = 2000 And c.[precision] = 0 And c.scale = 0 /*And c.is_nullable = 1*/)
		Alter Table [LOG_GERADOR] Alter Column [Arquivo_Gerado] varchar(2000) Null
Go

If Not Exists (Select 1 From sys.columns Where Name = 'tab_tipo_layout_id' And [object_Id] = Object_Id('LOG_GERADOR'))
	Alter Table [LOG_GERADOR] Add [tab_tipo_layout_id] int Null
Else 
	If Not Exists(Select * From sys.columns c Where c.[object_id] = Object_Id('LOG_GERADOR') And c.Name = 'tab_tipo_layout_id' And Type_Name(c.system_type_id) = 'int' And c.max_length = 4 And c.[precision] = 10 And c.scale = 0 /*And c.is_nullable = 1*/)
		Alter Table [LOG_GERADOR] Alter Column [tab_tipo_layout_id] int Null
Go
